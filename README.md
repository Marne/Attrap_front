# Attrap_front

Une [interface web](https://attrap.fr) pour faire des recherches dans les Recueils des actes administratifs (RAA) analysés par [Attrap](https://git.laquadrature.net/la-quadrature-du-net/Attrap)

## Installation

Il est recommandé d'utiliser virtualenv :

```bash
virtualenv --python=/usr/bin/python3 .
source bin/activate
pip3 install -r requirements.txt
```

Vous aurez également besoin d'une instance [OpenSearch](https://opensearch.org/) et Redis ou Valkey.

## Indexation des RAA

Avant la première indexation, vous devez créer l'index dans OpenSeach :

```
flask data create-index
```

Vous devez ensuite copier le dossier `data/` de Attrap dans ce dossier, puis indexer les fichiers :

```
flask data index-files
```

Vous pouvez également récupérer les fichiers depuis le S3 de la CI de Attrap avec les utilitaires `misc/download-from-s3.sh` et `misc/update-database.sh`.

## Développement

```bash
flask run --debug
```

Attrap_front sera disponible à l'adresse http://127.0.0.1:5000/

## Docker

Une image Docker peut être générée pour faire des tests. Il n'est pas recommandé de l'utiliser en production.

```bash
docker build -t attrap_front:latest -f misc/Dockerfile .
```

Lancement de l'image Docker :

```bash
touch ${PWD}/config.yml
docker run -d \
	--name attrap_front \
	--restart always \
	-v ${PWD}/data:/opt/Attrap_front/data \
	-v ${PWD}/opensearch-data:/var/lib/opensearch \
	-v ${PWD}/config.yml:/opt/Attrap_front/config.yml:ro \
	-p 127.0.0.1:8001:8000 \
	attrap_front:latest
```

Attrap_front sera disponible à l'adresse http://127.0.0.1:8001/. Vous pouvez ensuite configurer un reverse proxy pour atteindre cette adresse.

Pour créer l'index et indéxer les documents :

```bash
docker exec attrap_front flask data create-index
docker exec attrap_front flask data index-files
```

## Mise en production

Vous aurez besoin d'une WSGI et d'un reverse proxy pour mettre en production Attrap_front. Vous pouvez notamment utiliser gunicorn :

```bash
bin/gunicorn -w 4 'app:create_app()'
```

L'application écoutera ensuite sur le port 8000. Vous pouvez ensuite configurer nginx sur ce port. Il est recommandé de paramétrer un [rate_limit](http://nginx.org/en/docs/http/ngx_http_limit_req_module.html).

En créant un fichier `config.yml`, vous pouvez surcharger les variables par défaut de `config.default.yml`. La variable `MENTIONS_LEGALES` devrait au moins être surchargée.

## Licence

[CeCILL_V2.1-fr](https://cecill.info/licences/Licence_CeCILL_V2.1-fr.html) (voir le fichier `LICENSE`)
