#!/usr/bin/env bash

set -e

s3_key="${1}"
s3_secret="${2}"
s3_host="${3}"
s3_bucket="${4}"
dest="${5}"

root_path=$(dirname $(realpath "${BASH_SOURCE[0]}"))

administrations="ppparis
pref01
pref02
pref03
pref04
pref05
pref06
pref07
pref08
pref09
pref10
pref11
pref12
pref13
pref14
pref15
pref16
pref17
pref18
pref19
pref2A
pref2B
pref21
pref22
pref23
pref24
pref25
pref26
pref27
pref28
pref29
pref30
pref31
pref32
pref33
pref34
pref35
pref36
pref37
pref38
pref39
pref40
pref41
pref42
pref43
pref44
pref45
pref46
pref47
pref49
pref49
pref50
pref51
pref52
pref53
pref54
pref55
pref56
pref57
pref58
pref59
pref60
pref61
pref62
pref63
pref64
pref65
pref66
pref67
pref68
pref69
pref70
pref71
pref72
pref73
pref74
pref76
pref77
pref78
pref79
pref80
pref81
pref82
pref83
pref84
pref85
pref86
pref87
pref88
pref89
pref90
pref91
pref92
pref93
pref94
pref95
pref971
pref972
pref973
pref974
pref976"

if test -z "$s3_key" || test -z "$s3_secret" || test -z "$s3_host" || test -z "$s3_bucket" || test -z "$dest"; then
	echo "Usage: ${0} <s3_key> <s3_secret> <s3_host> <s3_bucket> <dest>"
	exit 1
fi

for i in $administrations; do
	${root_path}/download-from-s3.sh "${i}" "$s3_key" "$s3_secret" "$s3_host" "$s3_bucket" "$dest" || true
	rm "${dest}/${i}.zip" || true
done
