import random

from flask import Blueprint
from flask import current_app
from flask import render_template

from . import cache

bp = Blueprint("index", __name__)


@bp.route("/")
@cache.cached(timeout=5)
def index():
    alias_key=None
    if current_app.config['ALIASES']:
        alias_key, alias_value = random.choice(list(current_app.config['ALIASES'].items()))
    return render_template("index.html", alias_key=alias_key)

@bp.route("/mentions-legales")
@cache.cached(timeout=0)
def mentions_legales():
    return render_template("mentions-legales.html")

@bp.app_errorhandler(404)
def not_found(e):
    return render_template("404.html")

@bp.app_errorhandler(500)
def not_found(e):
    return render_template("500.html") 
