import re

from flask import Blueprint
from flask import current_app
from flask import jsonify
from flask import render_template
from flask import request
from flask import redirect
from flask import url_for

from . import cache

bp = Blueprint("search", __name__)


@bp.route('/search', methods=['GET'])
@cache.cached(timeout=0, query_string=True)
def search():
    search = request.args.get('s', '', type=str)
    size = 10
    try:
        size = int(request.args.get('size', '10'))
        if size > 50:
            size = 50
    except:
        size = 10

    page = 1
    try:
        page = int(request.args.get('page', '1'))
        if page <= 1:
            page = 1
    except:
        page = 1

    if search == '':
        return redirect(request.host_url, code=302)
    else:
        result = search_in_opensearch(search, size=size, page=page, pre_tags=['<mark>'], post_tags=['</mark>'])
        api_url = url_for('search.api_v1_search', s=search, _external=True)
        if size == 10:
            pager_url = url_for('search.search', s=search, _external=True)
        else:
            pager_url = url_for('search.search', s=search, size=size, _external=True)

        return render_template(
            'search/search.html',
            search=search,
            result=result,
            api_url=api_url,
            pager_url=pager_url,
            page=page,
            size=size
        )

@bp.route('/api/v1/search', methods=['GET'])
@cache.cached(timeout=0, query_string=True)
def api_v1_search():
    search = request.args.get('s', '', type=str)
    size = 10
    try:
        size = int(request.args.get('size', '10'))
        if size > 50:
            size = 50
    except:
        size = 10

    page = 1
    try:
        page = int(request.args.get('page', '1'))
        if page <= 1:
            page = 1
    except:
        page = 1

    return jsonify(search_in_opensearch(search, size=size, page=page))

def search_in_opensearch(search, size=10, page=0, pre_tags=None, post_tags=None):
    if not search or search == '':
        result = {
            'total': 0,
            'elements': []
        }
        return result
    else:
        query = {
            'from': (page-1)*size,
            'size': size,
            'query': {
                "query_string": {
                    "fields" : ['content'],
                    "query": f"{search}"
                }
            },
            "highlight": {
                "fields": {
                    "content": {"type": "plain"}
                }
            },
            "sort": [
                {
                    "date": {
                        "order": "desc"
                    }
                }
            ]
        }

        if pre_tags:
            query['highlight']['pre_tags'] = []
            for tag in pre_tags:
                query['highlight']['pre_tags'].append(tag)

        if post_tags:
            query['highlight']['post_tags'] = []
            for tag in post_tags:
                query['highlight']['post_tags'].append(tag)

        response = current_app.opensearch.search(
            body = query,
            index = 'attrap-index'
        )

        result = {
            'total': int(response['hits']['total']['value']),
            'elements': []
        }

        for raa in response['hits']['hits']:
            raa['_source']['content'] = []
            for content in raa['highlight']['content']:
                raa['_source']['content'].append(content.replace("\n", ' '))

            if current_app.config['ADMINISTRATIONS'].get(raa['_source']['administration']):
                raa['_source']['administration_name'] = current_app.config['ADMINISTRATIONS'].get(raa['_source']['administration'])
            else:
                raa['_source']['administration_name'] = raa['_source']['administration']
            result['elements'].append(raa['_source'])

        return result
