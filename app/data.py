import os
import sys
import glob
import re
import datetime

from flask import Blueprint
from flask import current_app

from opensearchpy.exceptions import RequestError

from . import cache

bp = Blueprint('data', __name__)


@bp.cli.command('create-index')
def create_index():
    print('Création de l\'index dans OpenSearch...')
    try:
        index_name = 'attrap-index'
        index_body = {
            'settings': {
                'index': {
                    'number_of_shards': 1,
                    'number_of_replicas': 1,
                    'highlight':{
                        'max_analyzed_offset': 10000000
                    }
                },
                'analysis': {
                    'analyzer': {
                        'attrap_analyzer': {
                            'tokenizer': 'standard',
                            'filter': [ 'attrap_stemmer', 'lowercase' ]
                        }
                    },
                    'filter': {
                        'attrap_stemmer': {
                            'type': 'stemmer',
                            'language': 'french'
                        }
                    }
                }
            },
            'mappings': {
                'properties': {
                    'content': {
                        'type': 'text',
                        'analyzer': 'attrap_analyzer'
                    },
                    "date": {
                        "type": "date",
                        "format" : "strict_date_optional_time"
                    }
                }
            }
        }
        response = current_app.opensearch.indices.create(index_name, body=index_body)
    except RequestError as exc:
        current_app.logger.error(f'Impossible de créer l\'index: {exc}')
        sys.exit(1)

@bp.cli.command('index-files')
def index_files():
    available_administrations = os.listdir(f'{current_app.root_path}/../data/')
    for administration in available_administrations:
        available_raa = glob.glob(os.path.join(f'{current_app.root_path}/../data/{administration}/raa/', '*.json'))
        for raa in available_raa:
            print(f'Indexation de {raa}')
            filename_root = re.sub('\\.json$', '', raa)
            if os.path.isfile(f'{filename_root}.ok'):
                current_app.logger.info(f'{raa} a déjà été indexé')
            else:
                json_content = current_app.json.load(open(raa))
                document_content = open(f'{filename_root}.txt').read()

                document = {
                    'name': json_content['name'],
                    'administration': administration,
                    'date': datetime.datetime.strptime(json_content['date'], "%d/%m/%Y").strftime("%Y-%m-%d"),
                    'url': json_content['url'],
                    'first_saw_on': json_content['first_saw_on'],
                    'pdf_creation_date': json_content['pdf_creation_date'],
                    'pdf_modification_date': json_content['pdf_modification_date'],
                    'content': document_content
                }

                response = current_app.opensearch.index(
                    index = 'attrap-index',
                    body = document,
                    refresh = True
                )

                f = open(f'{filename_root}.ok', 'a')
                f.write('')
                f.close()

                current_app.logger.info(response)
    print(f'Nettoyage du cache...')
    cache.clear()
