from flask import Blueprint
from flask import abort
from flask import current_app
from flask import redirect
from flask import url_for

bp = Blueprint("aliases", __name__)


@bp.route("/<alias>")
def alias(alias):
    if not current_app.config['ALIASES'].get(alias):
        abort(404)
    else:
        return redirect(url_for('search.search', s=current_app.config['ALIASES'][alias], _external=True))
