import os
import shutil
import yaml

from flask import Flask
from flask_caching import Cache

from werkzeug.middleware.proxy_fix import ProxyFix

from opensearchpy import OpenSearch

cache = Cache()


def create_app():
    app = Flask(__name__)
    app.root_path = os.path.dirname(os.path.abspath(__file__))
    app.config.from_file(f"{app.root_path}/../config.default.yml", load=config_yaml_loader, text=False)
    app.config.from_file(f"{app.root_path}/../config.yml", load=config_yaml_loader, silent=True, text=False)

    # Initialisation du cache
    cache.init_app(app)
    cache.clear()

    # Paramétrage du reverse proxy
    if app.config['DEBUG'] is False:
        app.wsgi_app = ProxyFix(app.wsgi_app, x_for=1, x_proto=1, x_host=1, x_prefix=1)

    # On copie le CSS et JS de Bootstrap dans le dossier public
    os.makedirs(f'{app.root_path}/static/vendors/bootstrap', exist_ok=True)
    shutil.copy(f'{app.root_path}/../vendors/bootstrap/dist/css/bootstrap.min.css', f'{app.root_path}/static/vendors/bootstrap/bootstrap.min.css')
    shutil.copy(f'{app.root_path}/../vendors/bootstrap/dist/css/bootstrap.min.css.map', f'{app.root_path}/static/vendors/bootstrap/bootstrap.min.css.map')
    shutil.copy(f'{app.root_path}/../vendors/bootstrap/dist/js/bootstrap.bundle.min.js', f'{app.root_path}/static/vendors/bootstrap/bootstrap.bundle.min.js')

    administrations = {}
    for administration in app.config['ADMINISTRATIONS']:
        administrations = administrations | administration
    app.config['ADMINISTRATIONS'] = administrations

    aliases = {}
    if app.config.get('ALIASES'):
        for alias in app.config['ALIASES']:
            aliases = aliases | alias
    app.config['ALIASES'] = aliases

    from . import index
    from . import search
    from . import data
    from . import aliases

    app.register_blueprint(index.bp)
    app.register_blueprint(search.bp)
    app.register_blueprint(data.bp)
    app.register_blueprint(aliases.bp)

    app.add_url_rule("/", endpoint="index")

    if not app.config['OPENSEARCH']['CA_CERTS'] == '':
        ca_certs = app.config['OPENSEARCH']['CA_CERTS']
    else:
        ca_certs = None

    if not app.config['OPENSEARCH']['AUTH']['CLIENT_CERT'] == '':
        client_cert = app.config['OPENSEARCH']['AUTH']['CLIENT_CERT']
    else:
        client_cert = None

    if not app.config['OPENSEARCH']['AUTH']['CLIENT_KEY'] == '':
        client_key = app.config['OPENSEARCH']['AUTH']['CLIENT_KEY']
    else:
        client_key = None

    app.opensearch = OpenSearch(
        hosts = [{
            'host': app.config['OPENSEARCH']['HOST'],
            'port': app.config['OPENSEARCH']['PORT']
        }],
        http_compress = app.config['OPENSEARCH']['HTTP_COMPRESS'],
        http_auth = (app.config['OPENSEARCH']['AUTH']['USERNAME'], app.config['OPENSEARCH']['AUTH']['PASSWORD']),
        use_ssl = app.config['OPENSEARCH']['USE_SSL'],
        verify_certs = app.config['OPENSEARCH']['VERIFY_CERTS'],
        ssl_assert_hostname = app.config['OPENSEARCH']['SSL_ASSERT_HOSTNAME'],
        ssl_show_warn = app.config['OPENSEARCH']['SSL_SHOW_WARN'],
        ca_certs = ca_certs,
        client_cert = client_cert,
        client_key = client_key,
    )

    return app

def config_yaml_loader(file):
    return yaml.load(file, Loader=yaml.FullLoader)
